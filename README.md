# Consultas en HiveQL

- Instalamos Apache Hadoop y Apache Hive
```
https://medium.com/@martintrujillo/instalaci%C3%B3n-de-apache-hadoop-y-hive-en-ubuntu-22-f8006bd20c9c
```
- Creamos el directorio user en hdfs
```
hdfs dfs -mkdir /user
```
- Subimos el fichero empleados.txt al hdfs
```
hdfs dfs -put cx_empleados.txt /user/
```
- Creamos una tabla empleados con los campos y tipos de datos definidos
```
create external table EMPLEADOS(EMPLOYEE_ID STRING,FIRST_NAME STRING ,LAST_NAME STRING,EMAIL STRING,PHONE_NUMBER STRING,HIRE_DATE DATE,JOB_ID INT,SALARY DECIMAL,DEPARTMENT_ID INT) row format delimited fields terminated by ',' stored as textfile ;
```
- Cargamos la tabla empleados con la data del archivo empleados.txt
```
load data inpath '/user/empleados.txt' into table empleados;
```
- Listamos el empleado_id, apellido, codigo de departamento, fecha contrato y el salario de cada empleado.
```
select employee_id,last_name,department_id,hire_date,salary from empleados;
```
- Calculamos el sueldo anual que percibe cada empleado
```
select employee_id,last_name,department_id,hire_date,salary,salary*12 AS SANUAL from empleados;
```
- Listamos los diferentes códigos de departamento.
```
SELECT DISTINCT department_id from empleados;
```
- Listamos el apellido y sueldo de los empleados que ganan más de 5000 y menos de 8000
```
select employee_id,last_name,department_id,hire_date,salary from empleados where salary between 5000 and 8000;
```
- Listamos el apellido, cod oficio y fecha contrato de empleados contratados entre 20 febrero 2008.
```
select employee_id,last_name,department_id,hire_date,salary from empleados where hire_date>'2008-02-20';
```
- Listamos el apellido y cod departamento de los empleados pertenecientes a los departamentos 80 o 50 y ordenar por apellido.
```
select employee_id,last_name,department_id,hire_date,salary from empleados where department_id in (50,80) order by last_name;
```
- Creamos una tabla empleado_opt con los campos: departamento_id, cantidad y total
```
create table empleados_opt (DEPARTMENT_ID INT,CANTIDAD INT,TOTAL DECIMAL);
```
- Insertamos el costo total por departamento, con la cantidad en esta nueva tabla utilizando el comando
```
INSERT INTO empleados_opt
select department_id,count(*),sum(salary) from empleados where department_id is not null group by department_id;
```
- Muestros las 3 primeras filas de esta nueva tabla
```
select * from empleados_opt limit 5;

```